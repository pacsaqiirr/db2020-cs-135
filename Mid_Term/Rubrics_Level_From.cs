﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Rubrics_Level_From : Form
    {
        public Rubrics_Level_From()
        {
            InitializeComponent();
            Load_RubricsIds_ToCombo();
            Load_M_level_ToCombo();
            showData();
        }
        private void Load_RubricsIds_ToCombo()
        {
            comboBox2.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric ", con);
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {
                s += DR[0].ToString() + "." + DR[1].ToString();
                comboBox2.Items.Add(s);
                s = "";
            }
            DR.Close();
        }
        private void Load_M_level_ToCombo()
        {
         for(int i=0;i<4;i++)
            {
                comboBox1.Items.Add(i + 1);
            }
        }
        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clear_Grid();
        }
        public void clear_Grid()
        {
            coursename.Text = "";
            comboBox1.Text = "Select Level";
            comboBox2.Text = "Select Rubric Id";
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            coursename.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string l= dataGridView1.CurrentRow.Cells[3].Value.ToString();
            comboBox2.Text = t;
            comboBox1.Text = l;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                Char iddd = '.';
                string[] id = comboBox2.SelectedItem.ToString().Split(iddd);
                var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into RubricLevel values (@RubricId,@Details,@MeasurementLevel)", con);
            cmd.Parameters.AddWithValue("@RubricId", int.Parse(id[0]));
            cmd.Parameters.AddWithValue("@Details", coursename.Text);
            cmd.Parameters.AddWithValue("@MeasurementLevel", comboBox1.SelectedItem.ToString());
            try
            {
                _ = cmd.ExecuteNonQuery();
                MessageBox.Show("Rubric Level saved");
            }
            catch { MessageBox.Show("Rubric Level Already Added"); }


        }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }
            showData();
              }
        public bool is_empty()
        {
            if (coursename.Text == "")
            {
                return false;
            }
            else if (comboBox1.Text == "Select Level")
            {
                return false;
            }
            else if (comboBox2.Text == "Select Rubric Id")
            {
                return false;
            }


            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                Char iddd = '.';
                string[] runricid = comboBox2.Text.Split(iddd);
                string[] leve = comboBox1.Text.Split(iddd);

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update RubricLevel Set RubricId=@RubricId,Details=@Details,MeasurementLevel=@MeasurementLevel where Id=@Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@RubricId", runricid[0].ToString());
                cmd.Parameters.AddWithValue("@Details", coursename.Text);
                cmd.Parameters.AddWithValue("@MeasurementLevel", leve[0].ToString());
                cmd.Parameters.AddWithValue("@Id", id);
                _ = cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            else
            {
                MessageBox.Show("Error! Some empty Field(s) ");
            }
            showData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete StudentResult where RubricMeasurementId=@RubricMeasurementId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@RubricMeasurementId", id);
            _ = cmd.ExecuteNonQuery();
           // MessageBox.Show("Successfully Deleted");


             con = Configuration.getInstance().getConnection();
             cmd = new SqlCommand("Delete RubricLevel where Id=@Id", con);
             da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@Id", id);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");

            showData();
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel where Details like '" + search_field.Text + "%' OR Id like '" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@Id", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    
}
