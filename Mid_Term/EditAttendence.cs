﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class EditAttendence : Form
    {
        public EditAttendence()
        {
            InitializeComponent();
            Show_Data();
            Load_Status_ToCombo();

           // Load_Course_ToCombo();
        }
        private void Load_Status_ToCombo()
        {
            DateTime datetime = DateTime.Now;
            var shortDateValue = datetime.ToShortDateString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select LookupId from Lookup where Category='ATTENDANCE_STATUS'", con);
            SqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                comboBox1.Items.Add(DR[0].ToString());
            }
            DR.Close();
        }
        private void Show_Data()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.Id,concat(s.FirstName,' ',s.LastName) as Name,s.RegistrationNumber,sa.AttendanceStatus,ca.Id as AttendanceId from StudentAttendance sa join ClassAttendance ca on ca.Id=sa.AttendanceId join Student s on s.Id=sa.StudentId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void EditAttendence_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime datetime = dateTimePicker1.Value;
            //MessageBox.Show(datetime.Date.ToShortDateString());
            var shortDateValue = datetime.ToShortDateString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.Id,concat(s.FirstName,' ',s.LastName) as Name,s.RegistrationNumber,sa.AttendanceStatus,ca.Id as AttendanceId from StudentAttendance sa join ClassAttendance ca on ca.Id=sa.AttendanceId join Student s on s.Id=sa.StudentId where AttendanceDate=@AttendanceDate", con);
            cmd.Parameters.AddWithValue("@AttendanceDate", datetime.Date);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);         
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DateTime datetime = dateTimePicker1.Value;
            var shortDateValue = datetime.ToShortDateString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select s.Id, concat(s.FirstName, ' ', s.LastName) as Name, s.RegistrationNumber, sa.AttendanceStatus, ca.Id as AttendanceId from StudentAttendance sa join ClassAttendance ca on ca.Id = sa.AttendanceId join Student s on s.Id = sa.StudentId  where ca.AttendanceDate=@AttendanceDate and s.RegistrationNumber like '%"+textBox1.Text+"%' or concat(s.FirstName,' ',s.LastName) like '%"+textBox1.Text+"%'", con);
            cmd.Parameters.AddWithValue("@AttendanceDate", datetime.Date);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Cells[3].Value.ToString() != "")
            {
                foreach (DataGridViewRow dr in dataGridView1.Rows)
                {
                    string id = dr.Cells[3].Value.ToString();                 
                    string sid = dr.Cells[0].Value.ToString();
                    string atteid = dr.Cells[4].Value.ToString();
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Update StudentAttendance set AttendanceStatus=@AttendanceStatus where AttendanceId=@AttendanceId and StudentId=@StudentId", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@StudentId", sid);
                    cmd.Parameters.AddWithValue("@AttendanceId", atteid);
                    cmd.Parameters.AddWithValue("@AttendanceStatus", int.Parse(id));
                    try
                    {
                        _ = cmd.ExecuteNonQuery();                    
                    }
                    catch (Exception ex){ MessageBox.Show("Nothing Changes"+ex); }

                }
            }
            MessageBox.Show("Successfully Update");
            Show_Data();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime datetime = dateTimePicker1.Value;
            var shortDateValue = datetime.ToShortDateString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete StudentAttendance where AttendanceId in (select Id from ClassAttendance where AttendanceDate=@AttendanceDate)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@AttendanceDate", datetime.Date);
            _ = cmd.ExecuteNonQuery();
           
             con = Configuration.getInstance().getConnection();
             cmd = new SqlCommand("Delete ClassAttendance where AttendanceDate=@AttendanceDate", con);
             da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@AttendanceDate", datetime.Date);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
            Show_Data();
        }

        private void comboBox1_SelectedIndexChanged_2(object sender, EventArgs e)
        {
            dataGridView1.CurrentRow.Cells[3].Value = comboBox1.SelectedItem.ToString();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            comboBox1.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
        }
    }
}
