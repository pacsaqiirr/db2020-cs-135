﻿
namespace Mid_Term
{
    partial class Teacher_Dashboard_Form
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Teacher_Dashboard_Form));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Side_Menu = new System.Windows.Forms.Panel();
            this.eva_sub_panel = new System.Windows.Forms.Panel();
            this.avaluate_butt = new System.Windows.Forms.Button();
            this.eva_main_butt = new System.Windows.Forms.Button();
            this.assess_subPanel = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.assess_butt = new System.Windows.Forms.Button();
            this.Assessment = new System.Windows.Forms.Button();
            this.Rubric_sun_Panel = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.rubric_Level_butt = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.CLOS_suboanel = new System.Windows.Forms.Panel();
            this.clos_button_sub = new System.Windows.Forms.Button();
            this.Main_CLOS_button = new System.Windows.Forms.Button();
            this.attendence_subpanel = new System.Windows.Forms.Panel();
            this.edit_attendence = new System.Windows.Forms.Button();
            this.mark_attendence = new System.Windows.Forms.Button();
            this.Attendence_button = new System.Windows.Forms.Button();
            this.student_subsection = new System.Windows.Forms.Panel();
            this.add_student = new System.Windows.Forms.Button();
            this.student_Section = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Dashboard = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.controlPanel = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.Side_Menu.SuspendLayout();
            this.eva_sub_panel.SuspendLayout();
            this.assess_subPanel.SuspendLayout();
            this.Rubric_sun_Panel.SuspendLayout();
            this.CLOS_suboanel.SuspendLayout();
            this.attendence_subpanel.SuspendLayout();
            this.student_subsection.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(20)))));
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(814, 34);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Right;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button3.Location = new System.Drawing.Point(718, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(31, 34);
            this.button3.TabIndex = 4;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(749, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(31, 34);
            this.button2.TabIndex = 3;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(780, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 34);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Side_Menu
            // 
            this.Side_Menu.AutoScroll = true;
            this.Side_Menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.Side_Menu.Controls.Add(this.eva_sub_panel);
            this.Side_Menu.Controls.Add(this.eva_main_butt);
            this.Side_Menu.Controls.Add(this.assess_subPanel);
            this.Side_Menu.Controls.Add(this.Assessment);
            this.Side_Menu.Controls.Add(this.Rubric_sun_Panel);
            this.Side_Menu.Controls.Add(this.button5);
            this.Side_Menu.Controls.Add(this.CLOS_suboanel);
            this.Side_Menu.Controls.Add(this.Main_CLOS_button);
            this.Side_Menu.Controls.Add(this.attendence_subpanel);
            this.Side_Menu.Controls.Add(this.Attendence_button);
            this.Side_Menu.Controls.Add(this.student_subsection);
            this.Side_Menu.Controls.Add(this.student_Section);
            this.Side_Menu.Controls.Add(this.flowLayoutPanel1);
            this.Side_Menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.Side_Menu.Location = new System.Drawing.Point(0, 34);
            this.Side_Menu.Name = "Side_Menu";
            this.Side_Menu.Size = new System.Drawing.Size(179, 465);
            this.Side_Menu.TabIndex = 1;
            // 
            // eva_sub_panel
            // 
            this.eva_sub_panel.Controls.Add(this.button6);
            this.eva_sub_panel.Controls.Add(this.avaluate_butt);
            this.eva_sub_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.eva_sub_panel.Location = new System.Drawing.Point(0, 814);
            this.eva_sub_panel.Name = "eva_sub_panel";
            this.eva_sub_panel.Size = new System.Drawing.Size(162, 90);
            this.eva_sub_panel.TabIndex = 21;
            // 
            // avaluate_butt
            // 
            this.avaluate_butt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.avaluate_butt.Dock = System.Windows.Forms.DockStyle.Top;
            this.avaluate_butt.FlatAppearance.BorderSize = 0;
            this.avaluate_butt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.avaluate_butt.ForeColor = System.Drawing.SystemColors.Control;
            this.avaluate_butt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.avaluate_butt.Location = new System.Drawing.Point(0, 0);
            this.avaluate_butt.Name = "avaluate_butt";
            this.avaluate_butt.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.avaluate_butt.Size = new System.Drawing.Size(162, 45);
            this.avaluate_butt.TabIndex = 1;
            this.avaluate_butt.Text = "Evaluate Students";
            this.avaluate_butt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.avaluate_butt.UseVisualStyleBackColor = false;
            this.avaluate_butt.Click += new System.EventHandler(this.avaluate_butt_Click);
            // 
            // eva_main_butt
            // 
            this.eva_main_butt.Dock = System.Windows.Forms.DockStyle.Top;
            this.eva_main_butt.FlatAppearance.BorderSize = 0;
            this.eva_main_butt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eva_main_butt.ForeColor = System.Drawing.SystemColors.Control;
            this.eva_main_butt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.eva_main_butt.Location = new System.Drawing.Point(0, 769);
            this.eva_main_butt.Name = "eva_main_butt";
            this.eva_main_butt.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.eva_main_butt.Size = new System.Drawing.Size(162, 45);
            this.eva_main_butt.TabIndex = 20;
            this.eva_main_butt.Text = "Evaluation";
            this.eva_main_butt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.eva_main_butt.UseVisualStyleBackColor = true;
            this.eva_main_butt.Click += new System.EventHandler(this.eva_main_butt_Click);
            // 
            // assess_subPanel
            // 
            this.assess_subPanel.Controls.Add(this.button4);
            this.assess_subPanel.Controls.Add(this.assess_butt);
            this.assess_subPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.assess_subPanel.Location = new System.Drawing.Point(0, 679);
            this.assess_subPanel.Name = "assess_subPanel";
            this.assess_subPanel.Size = new System.Drawing.Size(162, 90);
            this.assess_subPanel.TabIndex = 19;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.SystemColors.Control;
            this.button4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button4.Location = new System.Drawing.Point(0, 45);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.button4.Size = new System.Drawing.Size(162, 45);
            this.button4.TabIndex = 2;
            this.button4.Text = "Components";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_2);
            // 
            // assess_butt
            // 
            this.assess_butt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.assess_butt.Dock = System.Windows.Forms.DockStyle.Top;
            this.assess_butt.FlatAppearance.BorderSize = 0;
            this.assess_butt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.assess_butt.ForeColor = System.Drawing.SystemColors.Control;
            this.assess_butt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.assess_butt.Location = new System.Drawing.Point(0, 0);
            this.assess_butt.Name = "assess_butt";
            this.assess_butt.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.assess_butt.Size = new System.Drawing.Size(162, 45);
            this.assess_butt.TabIndex = 1;
            this.assess_butt.Text = "Assessments";
            this.assess_butt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.assess_butt.UseVisualStyleBackColor = false;
            this.assess_butt.Click += new System.EventHandler(this.assess_butt_Click);
            // 
            // Assessment
            // 
            this.Assessment.Dock = System.Windows.Forms.DockStyle.Top;
            this.Assessment.FlatAppearance.BorderSize = 0;
            this.Assessment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Assessment.ForeColor = System.Drawing.SystemColors.Control;
            this.Assessment.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Assessment.Location = new System.Drawing.Point(0, 634);
            this.Assessment.Name = "Assessment";
            this.Assessment.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.Assessment.Size = new System.Drawing.Size(162, 45);
            this.Assessment.TabIndex = 18;
            this.Assessment.Text = "Assessment Section";
            this.Assessment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Assessment.UseVisualStyleBackColor = true;
            this.Assessment.Click += new System.EventHandler(this.Assessment_Click);
            // 
            // Rubric_sun_Panel
            // 
            this.Rubric_sun_Panel.Controls.Add(this.button7);
            this.Rubric_sun_Panel.Controls.Add(this.rubric_Level_butt);
            this.Rubric_sun_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.Rubric_sun_Panel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Rubric_sun_Panel.Location = new System.Drawing.Point(0, 544);
            this.Rubric_sun_Panel.Name = "Rubric_sun_Panel";
            this.Rubric_sun_Panel.Size = new System.Drawing.Size(162, 90);
            this.Rubric_sun_Panel.TabIndex = 17;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.button7.Dock = System.Windows.Forms.DockStyle.Top;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.SystemColors.Control;
            this.button7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button7.Location = new System.Drawing.Point(0, 45);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.button7.Size = new System.Drawing.Size(162, 45);
            this.button7.TabIndex = 4;
            this.button7.Text = "Rubrics Level";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // rubric_Level_butt
            // 
            this.rubric_Level_butt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.rubric_Level_butt.Dock = System.Windows.Forms.DockStyle.Top;
            this.rubric_Level_butt.FlatAppearance.BorderSize = 0;
            this.rubric_Level_butt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rubric_Level_butt.ForeColor = System.Drawing.SystemColors.Control;
            this.rubric_Level_butt.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rubric_Level_butt.Location = new System.Drawing.Point(0, 0);
            this.rubric_Level_butt.Name = "rubric_Level_butt";
            this.rubric_Level_butt.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.rubric_Level_butt.Size = new System.Drawing.Size(162, 45);
            this.rubric_Level_butt.TabIndex = 2;
            this.rubric_Level_butt.Text = "Rubrics";
            this.rubric_Level_butt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rubric_Level_butt.UseVisualStyleBackColor = false;
            this.rubric_Level_butt.Click += new System.EventHandler(this.rubric_Level_butt_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.SystemColors.Control;
            this.button5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button5.Location = new System.Drawing.Point(0, 499);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(162, 45);
            this.button5.TabIndex = 16;
            this.button5.Text = "Rubrics Section";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // CLOS_suboanel
            // 
            this.CLOS_suboanel.Controls.Add(this.clos_button_sub);
            this.CLOS_suboanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CLOS_suboanel.Location = new System.Drawing.Point(0, 454);
            this.CLOS_suboanel.Name = "CLOS_suboanel";
            this.CLOS_suboanel.Size = new System.Drawing.Size(162, 45);
            this.CLOS_suboanel.TabIndex = 15;
            // 
            // clos_button_sub
            // 
            this.clos_button_sub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.clos_button_sub.Dock = System.Windows.Forms.DockStyle.Top;
            this.clos_button_sub.FlatAppearance.BorderSize = 0;
            this.clos_button_sub.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clos_button_sub.ForeColor = System.Drawing.SystemColors.Control;
            this.clos_button_sub.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.clos_button_sub.Location = new System.Drawing.Point(0, 0);
            this.clos_button_sub.Name = "clos_button_sub";
            this.clos_button_sub.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.clos_button_sub.Size = new System.Drawing.Size(162, 45);
            this.clos_button_sub.TabIndex = 3;
            this.clos_button_sub.Text = "Manage CLOS";
            this.clos_button_sub.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.clos_button_sub.UseVisualStyleBackColor = false;
            this.clos_button_sub.Click += new System.EventHandler(this.clos_button_sub_Click);
            // 
            // Main_CLOS_button
            // 
            this.Main_CLOS_button.Dock = System.Windows.Forms.DockStyle.Top;
            this.Main_CLOS_button.FlatAppearance.BorderSize = 0;
            this.Main_CLOS_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Main_CLOS_button.ForeColor = System.Drawing.SystemColors.Control;
            this.Main_CLOS_button.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Main_CLOS_button.Location = new System.Drawing.Point(0, 409);
            this.Main_CLOS_button.Name = "Main_CLOS_button";
            this.Main_CLOS_button.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.Main_CLOS_button.Size = new System.Drawing.Size(162, 45);
            this.Main_CLOS_button.TabIndex = 9;
            this.Main_CLOS_button.Text = "CLOS Section";
            this.Main_CLOS_button.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Main_CLOS_button.UseVisualStyleBackColor = true;
            this.Main_CLOS_button.Click += new System.EventHandler(this.Main_CLOS_button_Click);
            // 
            // attendence_subpanel
            // 
            this.attendence_subpanel.Controls.Add(this.edit_attendence);
            this.attendence_subpanel.Controls.Add(this.mark_attendence);
            this.attendence_subpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.attendence_subpanel.Location = new System.Drawing.Point(0, 319);
            this.attendence_subpanel.Name = "attendence_subpanel";
            this.attendence_subpanel.Size = new System.Drawing.Size(162, 90);
            this.attendence_subpanel.TabIndex = 8;
            // 
            // edit_attendence
            // 
            this.edit_attendence.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.edit_attendence.Dock = System.Windows.Forms.DockStyle.Top;
            this.edit_attendence.FlatAppearance.BorderSize = 0;
            this.edit_attendence.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.edit_attendence.ForeColor = System.Drawing.SystemColors.Control;
            this.edit_attendence.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.edit_attendence.Location = new System.Drawing.Point(0, 45);
            this.edit_attendence.Name = "edit_attendence";
            this.edit_attendence.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.edit_attendence.Size = new System.Drawing.Size(162, 45);
            this.edit_attendence.TabIndex = 2;
            this.edit_attendence.Text = "Edit Attendence";
            this.edit_attendence.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.edit_attendence.UseVisualStyleBackColor = false;
            this.edit_attendence.Click += new System.EventHandler(this.edit_attendence_Click);
            // 
            // mark_attendence
            // 
            this.mark_attendence.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.mark_attendence.Dock = System.Windows.Forms.DockStyle.Top;
            this.mark_attendence.FlatAppearance.BorderSize = 0;
            this.mark_attendence.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mark_attendence.ForeColor = System.Drawing.SystemColors.Control;
            this.mark_attendence.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.mark_attendence.Location = new System.Drawing.Point(0, 0);
            this.mark_attendence.Name = "mark_attendence";
            this.mark_attendence.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.mark_attendence.Size = new System.Drawing.Size(162, 45);
            this.mark_attendence.TabIndex = 1;
            this.mark_attendence.Text = "Mark Attendence";
            this.mark_attendence.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mark_attendence.UseVisualStyleBackColor = false;
            this.mark_attendence.Click += new System.EventHandler(this.mark_attendence_Click);
            // 
            // Attendence_button
            // 
            this.Attendence_button.Dock = System.Windows.Forms.DockStyle.Top;
            this.Attendence_button.FlatAppearance.BorderSize = 0;
            this.Attendence_button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Attendence_button.ForeColor = System.Drawing.SystemColors.Control;
            this.Attendence_button.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Attendence_button.Location = new System.Drawing.Point(0, 274);
            this.Attendence_button.Name = "Attendence_button";
            this.Attendence_button.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.Attendence_button.Size = new System.Drawing.Size(162, 45);
            this.Attendence_button.TabIndex = 7;
            this.Attendence_button.Text = "Attendence Section";
            this.Attendence_button.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Attendence_button.UseVisualStyleBackColor = true;
            this.Attendence_button.Click += new System.EventHandler(this.Attendence_button_Click);
            // 
            // student_subsection
            // 
            this.student_subsection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.student_subsection.Controls.Add(this.add_student);
            this.student_subsection.Dock = System.Windows.Forms.DockStyle.Top;
            this.student_subsection.Location = new System.Drawing.Point(0, 229);
            this.student_subsection.Name = "student_subsection";
            this.student_subsection.Size = new System.Drawing.Size(162, 45);
            this.student_subsection.TabIndex = 4;
            // 
            // add_student
            // 
            this.add_student.Dock = System.Windows.Forms.DockStyle.Top;
            this.add_student.FlatAppearance.BorderSize = 0;
            this.add_student.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add_student.ForeColor = System.Drawing.SystemColors.Control;
            this.add_student.Location = new System.Drawing.Point(0, 0);
            this.add_student.Name = "add_student";
            this.add_student.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.add_student.Size = new System.Drawing.Size(162, 45);
            this.add_student.TabIndex = 0;
            this.add_student.Text = "Student\'s CRUD ";
            this.add_student.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.add_student.UseVisualStyleBackColor = true;
            this.add_student.Click += new System.EventHandler(this.add_student_Click);
            // 
            // student_Section
            // 
            this.student_Section.Dock = System.Windows.Forms.DockStyle.Top;
            this.student_Section.FlatAppearance.BorderSize = 0;
            this.student_Section.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.student_Section.ForeColor = System.Drawing.SystemColors.Control;
            this.student_Section.Location = new System.Drawing.Point(0, 184);
            this.student_Section.Name = "student_Section";
            this.student_Section.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.student_Section.Size = new System.Drawing.Size(162, 45);
            this.student_Section.TabIndex = 3;
            this.student_Section.Text = "Manage Student";
            this.student_Section.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.student_Section.UseVisualStyleBackColor = true;
            this.student_Section.Click += new System.EventHandler(this.button4_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pictureBox1);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.Dashboard);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(162, 184);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(176, 65);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(3, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "Rubrics Based Evaluation";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Dashboard
            // 
            this.Dashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.Dashboard.FlatAppearance.BorderSize = 0;
            this.Dashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Dashboard.ForeColor = System.Drawing.SystemColors.Control;
            this.Dashboard.Location = new System.Drawing.Point(3, 114);
            this.Dashboard.Name = "Dashboard";
            this.Dashboard.Size = new System.Drawing.Size(179, 64);
            this.Dashboard.TabIndex = 0;
            this.Dashboard.Text = "Dashboard";
            this.Dashboard.UseVisualStyleBackColor = true;
            this.Dashboard.Click += new System.EventHandler(this.Dashboard_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(35)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(179, 422);
            this.panel3.Name = "panel3";
            this.panel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel3.Size = new System.Drawing.Size(635, 77);
            this.panel3.TabIndex = 2;
            // 
            // controlPanel
            // 
            this.controlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlPanel.Location = new System.Drawing.Point(179, 34);
            this.controlPanel.Name = "controlPanel";
            this.controlPanel.Size = new System.Drawing.Size(635, 388);
            this.controlPanel.TabIndex = 3;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.SystemColors.Control;
            this.button6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button6.Location = new System.Drawing.Point(0, 45);
            this.button6.Name = "button6";
            this.button6.Padding = new System.Windows.Forms.Padding(25, 0, 0, 0);
            this.button6.Size = new System.Drawing.Size(162, 45);
            this.button6.TabIndex = 2;
            this.button6.Text = "Create Reporrt";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // Teacher_Dashboard_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(44)))));
            this.ClientSize = new System.Drawing.Size(814, 499);
            this.Controls.Add(this.controlPanel);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.Side_Menu);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Teacher_Dashboard_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LMS";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_Dashboard_Form_MouseDown);
            this.panel1.ResumeLayout(false);
            this.Side_Menu.ResumeLayout(false);
            this.eva_sub_panel.ResumeLayout(false);
            this.assess_subPanel.ResumeLayout(false);
            this.Rubric_sun_Panel.ResumeLayout(false);
            this.CLOS_suboanel.ResumeLayout(false);
            this.attendence_subpanel.ResumeLayout(false);
            this.student_subsection.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel Side_Menu;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel student_subsection;
        private System.Windows.Forms.Button student_Section;
        private System.Windows.Forms.Panel attendence_subpanel;
        private System.Windows.Forms.Button edit_attendence;
        private System.Windows.Forms.Button mark_attendence;
        private System.Windows.Forms.Button Attendence_button;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button Dashboard;
        private System.Windows.Forms.Panel CLOS_suboanel;
        private System.Windows.Forms.Button add_student;
        private System.Windows.Forms.Button Main_CLOS_button;
        private System.Windows.Forms.Button clos_button_sub;
        private System.Windows.Forms.Panel eva_sub_panel;
        private System.Windows.Forms.Button eva_main_butt;
        private System.Windows.Forms.Panel assess_subPanel;
        private System.Windows.Forms.Button assess_butt;
        private System.Windows.Forms.Button Assessment;
        private System.Windows.Forms.Panel Rubric_sun_Panel;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button rubric_Level_butt;
        private System.Windows.Forms.Panel controlPanel;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button avaluate_butt;
        private System.Windows.Forms.Button button6;
    }
}

