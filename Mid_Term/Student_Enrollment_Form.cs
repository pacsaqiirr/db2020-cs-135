﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Student_Enrollment_Form : Form
    {
        public Student_Enrollment_Form()
        {
            InitializeComponent();
            showData();
            Load_Course_ToCombo();
        }
        private void Load_Course_ToCombo()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Course ", con);
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox1.Items.Add(DR[0]);

            }
            DR.Close();
        }
        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            comboBox2.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Enrollments where StudentRegNo=@StudentRegNo", con);
            cmd.Parameters.AddWithValue("@StudentRegNo", dataGridView1.CurrentRow.Cells[0].Value.ToString());
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox2.Items.Add(DR[1]);

            }
            DR.Close();
        }
        public bool is_empty()
        {
            if (dataGridView1.CurrentRow.Cells[0].Value.ToString() =="" || comboBox1.Text == "" || comboBox1.Text == "Select Course")
            {
                return false;
            }


            return true;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Enrollments values (@StudentRegNo, @CourseName)", con);
                cmd.Parameters.AddWithValue("@StudentRegNo", dataGridView1.CurrentRow.Cells[0].Value.ToString());
                cmd.Parameters.AddWithValue("@CourseName", comboBox1.Text);
               
                try
                {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                }
                catch { MessageBox.Show("Student Already Enrolled"); }


            }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete Enrollments where CourseName=@CourseName", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@CourseName", comboBox2.Text);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully UnRegistered");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            comboBox2.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Enrollments where StudentRegNo=@StudentRegNo", con);
            cmd.Parameters.AddWithValue("@StudentRegNo", dataGridView1.CurrentRow.Cells[0].Value.ToString());
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox2.Items.Add(DR[1]);

            }
            DR.Close();
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student where RegistrationNumber like '" + search_field.Text + "%' OR Name like '" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
