﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{

    
    public partial class Teacher_Dashboard_Form : Form
    {
        private bool flag = false;
        private Form actve_Form;

        public Teacher_Dashboard_Form()
        {
            InitializeComponent();
            hide_all_Submenu();
            Child_Form_Show(new Dashoboard());
            this.ControlBox = false;
            this.DoubleBuffered=true;
            this.MaximizedBounds=Screen.FromHandle(this.Handle).WorkingArea;

        }
        private void hide_all_Submenu()
        {
            student_subsection.Visible = false;
            attendence_subpanel.Visible = false;
            CLOS_suboanel.Visible = false;
            Rubric_sun_Panel.Visible = false;
            eva_sub_panel.Visible = false;
            assess_subPanel.Visible = false;
        }

        private void hide_if_active()
        {
            if(student_subsection.Visible == true)
            {
                student_subsection.Visible = false;
            }

           

            if (attendence_subpanel.Visible == true)
            {
                attendence_subpanel.Visible = false;
            }


            if (assess_subPanel.Visible == true)
            {
                assess_subPanel.Visible = false;
            }

            if (eva_sub_panel.Visible == true)
            {
                eva_sub_panel.Visible = false;
            }

            if (CLOS_suboanel.Visible == true)
            {
                CLOS_suboanel.Visible = false;
            }
            if (Rubric_sun_Panel.Visible == true)
            {
                Rubric_sun_Panel.Visible = false;
            }
        }

        private void show_one_submenu(Panel name)
        {
            if(name.Visible==false)
            {
                hide_if_active();
                name.Visible = true;
            }
            else
            {
                name.Visible = false; 
            }
        }


        private void Child_Form_Show(Form form)
        {
            if(actve_Form != null)
            {
                actve_Form.Close();
            }
            actve_Form = form;
            form.TopLevel = false;
            form.FormBorderStyle = FormBorderStyle.None;
            form.Dock = DockStyle.Fill;
            controlPanel.Controls.Add(form);
            controlPanel.Tag = form;
            form.BringToFront();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (flag == false)
            {
                WindowState = FormWindowState.Maximized;
                flag = true;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                flag = false;

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {


            WindowState = FormWindowState.Minimized;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            show_one_submenu(student_subsection);
        }

        private void add_student_Click(object sender, EventArgs e)
        {
          
            Child_Form_Show(new Student_Form());
            hide_all_Submenu();
        }

        private void student_enrollment_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new Student_Enrollment_Form());
            hide_all_Submenu();
        }

        private void course_Section_Click(object sender, EventArgs e)
        {
           

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new CoursesForm());
            hide_all_Submenu();
        }

        private void Attendence_button_Click(object sender, EventArgs e)
        {
            show_one_submenu(attendence_subpanel);
        }

        private void mark_attendence_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new Mark_Attendence_Form());
            hide_all_Submenu();
        }

        private void edit_attendence_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new EditAttendence());
            hide_all_Submenu();
        }

        private void Main_Dashboard_Form_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
    {
        ReleaseCapture();
        SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
    }
        }

        private void Dashboard_Click(object sender, EventArgs e)
        {

            Child_Form_Show(new Dashoboard());
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Child_Form_Show(new CoursesEnrollment());
            hide_all_Submenu();
        }

        private void Main_CLOS_button_Click(object sender, EventArgs e)
        {
            show_one_submenu(CLOS_suboanel);
        }

        private void clos_button_sub_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new CLOS_Form());
            hide_all_Submenu();
        }

        private void rubric_Level_butt_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new Rubrics_Form());
            hide_all_Submenu();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new Rubrics_Level_From());
            hide_all_Submenu();
        }

        private void assess_butt_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new Assessment_Form());
            hide_all_Submenu();
        }

        private void avaluate_butt_Click(object sender, EventArgs e)
        {
            Child_Form_Show(new Evaluation_Form());
            hide_all_Submenu();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            show_one_submenu(Rubric_sun_Panel);
        }

        private void Assessment_Click(object sender, EventArgs e)
        {
           
            show_one_submenu(assess_subPanel);
        }

        private void eva_main_butt_Click(object sender, EventArgs e)
        {
            show_one_submenu(eva_sub_panel);
        }

        private void button4_Click_2(object sender, EventArgs e)
        {
            Child_Form_Show(new Assessment_Component_Form());
            hide_all_Submenu();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            Child_Form_Show(new Report());
            hide_all_Submenu();
        }
    }
}
