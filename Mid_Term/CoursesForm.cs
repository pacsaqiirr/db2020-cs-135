﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class CoursesForm : Form
    {
        public CoursesForm()
        {
            InitializeComponent();
            showData();

        }
        public void clear_Grid()
        {
            courseid.Text = "";
            coursename.Text = "";
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clear_Grid();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Course values (@Name,@Code)", con);
                cmd.Parameters.AddWithValue("@Name", coursename.Text);
                cmd.Parameters.AddWithValue("@Code", courseid.Text);
                try
                {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                }
                catch { MessageBox.Show("Course Already Added"); }


            }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }
            showData();
        }
        public bool is_empty()
        {
            if (courseid.Text == "" || coursename.Text == "" )
            {
                return false;
            }


            return true;
        }

        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Course", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete Course where Code=@Code", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@Code", courseid.Text);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");

            showData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Course Set Name=@Name,Code=@Code where Code=@Code", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@Code", courseid.Text);
                cmd.Parameters.AddWithValue("@Name", coursename.Text);
                _ = cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            else
            {
                MessageBox.Show("Error! Some empty Field(s) ");
            }
            showData();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            coursename.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            courseid.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
           
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Course where Code like '" + search_field.Text + "%' OR Name like '" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@Code", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
