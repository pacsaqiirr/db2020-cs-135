﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Rubrics_Form : Form
    {
        public Rubrics_Form()
        {
            InitializeComponent();
            showData();
            Load_Rubrics_ToCombo();
        }
        public void deletRubric()
        {
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete StudentResult where AssessmentComponentId in (select Id from AssessmentComponent where RubricId=@RubricId)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@RubricId", id);
            _ = cmd.ExecuteNonQuery();

            //ASSESSMENT COMPONENT
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete AssessmentComponent where RubricId=@RubricId", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@RubricId", id);
            _ = cmd.ExecuteNonQuery();

            //RUBRIC LEVEL
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete RubricLevel where RubricId=@RubricId", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@RubricId", id);
            _ = cmd.ExecuteNonQuery();

            //DELELTING RUBRICS
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete Rubric where Id=@Id", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@Id", id);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");

            showData();
        }
        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        private void Load_Rubrics_ToCombo()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo ", con);
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {             
                s += DR[0].ToString() + "."+DR[1].ToString();
                comboBox2.Items.Add(s);
                s = "";
            }
          
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clear_Grid();
        }
        public void clear_Grid()
        {
            coursename.Text = "";
            comboBox2.Text = "Select CLO";
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            coursename.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            comboBox2.Text = t;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                string s = "";
               var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select max(Id) from Rubric", con);
                SqlDataReader DR = cmd.ExecuteReader();
                while (DR.Read())
                {
                    s = DR[0].ToString();
                  //  MessageBox.Show(s);
                }
                DR.Close();
                Char iddd = '.';
                string[] id = comboBox2.Text.Split(iddd);
                foreach(string d in id)
                {
                    //MessageBox.Show(d);
                }
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("Insert into Rubric values (@Id,@Details,@CloId)", con);
                cmd.Parameters.AddWithValue("@Id", int.Parse(s)+1);
                cmd.Parameters.AddWithValue("@Details",coursename.Text);
                cmd.Parameters.AddWithValue("@CloId",int.Parse(id[0]));
                try
                {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Rubric saved");
                }
                catch { MessageBox.Show("Rubric Already Added"); }


            }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }
            showData();
        }
        public bool is_empty()
        {
            if (coursename.Text == "")
            {
                return false;
            }
            else if(comboBox2.Text=="Select CLO")
                {
                return false;
            }

            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                Char iddd = '.';
                string[] cloid= comboBox2.Text.Split(iddd);
                string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Rubric Set Details=@Details,CloId=@CloId where Id=@Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@Details", coursename.Text);
                cmd.Parameters.AddWithValue("@CloId", int.Parse(cloid[0]));
                cmd.Parameters.AddWithValue("@Id", id);
                _ = cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            else
            {
                MessageBox.Show("Error! Some empty Field(s) ");
            }
            showData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            deletRubric();
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric where Details like '" + search_field.Text + "%' OR Id like '" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@Id", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
