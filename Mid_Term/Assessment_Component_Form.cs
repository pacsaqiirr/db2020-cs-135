﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Assessment_Component_Form : Form
    {
        public Assessment_Component_Form()
        {
            InitializeComponent();
            showData();
            Load_Rubricid_ToCombo();
            Load_Assessmrntid_ToCombo();
        }
        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Load_Rubricid_ToCombo()
        {
            rubriccombo.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric ", con);
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {
                s += DR[0].ToString() + "." + DR[1].ToString();
                rubriccombo.Items.Add(s);
                s = "";
            }
            DR.Close();
        }
        private void Load_Assessmrntid_ToCombo()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment ", con);
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {
                s += DR[0].ToString() + "." + DR[1].ToString();
                assesscombo.Items.Add(s);
                s = "";
            }        
            DR.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clear_Grid();
        }
        public void clear_Grid()
        {
            coursename.Text = "";
            textBox2.Text = "";
            assesscombo.Text = "Select Id";
            rubriccombo.Text = "Select Id";
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            coursename.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            string t = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            string s = dataGridView1.CurrentRow.Cells[6].Value.ToString();
            rubriccombo.Text = t;
            assesscombo.Text = s;
            textBox2.Text= dataGridView1.CurrentRow.Cells[3].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int marks = ass();
            int assesmarks = AssessmentMarks();
         //   MessageBox.Show("Total Assess Marks " + assesmarks.ToString());
           // MessageBox.Show("Total AssessCompo Marks " + marks.ToString());
            if (marks > assesmarks)
            {
                MessageBox.Show("The Assessment Marks Exceed The Total Marks Limit");
            }
            else
            {
                if (is_empty())
                {
                    Char iddd = '.';
                    string[] id = rubriccombo.Text.Split(iddd);
                    string[] id2= assesscombo.Text.Split(iddd);
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name,@RubricId,@TotalMarks,@DateCreated,@DateUpdated,@AssessmentId)", con);
                    cmd.Parameters.AddWithValue("@Name", coursename.Text);
                    cmd.Parameters.AddWithValue("@RubricId", int.Parse(id[0]));
                    cmd.Parameters.AddWithValue("@TotalMarks", textBox2.Text);
                    cmd.Parameters.AddWithValue("@DateCreated", DateTime.Today);
                    cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Today);
                    cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(id2[0]));
                    cmd.Parameters.AddWithValue("@TotalWeightage", textBox2.Text);
                    try
                    {
                        _ = cmd.ExecuteNonQuery();
                        MessageBox.Show("Assessment Component saved");
                    }
                    catch { MessageBox.Show("Assessment Already Added"); }


                }
                else
                {
                    MessageBox.Show("Some empty Field(s) ");
                }
            }
            showData();
        }
        public bool is_empty()
        {
            if (coursename.Text == "")
            {
                return false;
            }
            else if (textBox2.Text == "")
            {
                return false;
            }
            else if (assesscombo.Text == "Select Id")
            {
                return false;
            }
            else if (rubriccombo.Text == "Select Id")
            {
                return false;
            }

            return true;
        }
        public int ass()
        {
            Char iddd = '.';
            string[] id = assesscombo.Text.Split(iddd);
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select sum(TotalMarks) from AssessmentComponent where AssessmentId=@AssessmentId", con);
            cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(id[0]));
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {
              s=DR[0].ToString();
            //  MessageBox.Show(s);
            }
            DR.Close();
            if(s=="")
            {
                return int.Parse(textBox2.Text);
            }
            int i = int.Parse(s)+ int.Parse(textBox2.Text);
            return i;
        }
        public int AssessmentMarks()
        {
            Char iddd = '.';
            string[] id = assesscombo.Text.Split(iddd);
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select TotalMarks from Assessment where Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", int.Parse(id[0]));
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {
                s = DR[0].ToString();
            }
            DR.Close();
            int i = int.Parse(s);
            return i;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete StudentResult where AssessmentComponentId=@AssessmentComponentId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@AssessmentComponentId", id);
            _ = cmd.ExecuteNonQuery();
//            MessageBox.Show("Successfully Deleted");

            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete AssessmentComponent where Id=@Id", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@Id", id);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");

            showData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int marks = ass();
            int assesmarks = AssessmentMarks();
            if (marks > assesmarks)
            {
                MessageBox.Show("The Assessment Marks Exceed The Total Marks Limit");
            }
            else
            {
                if (is_empty())
                {
                    Char iddd = '.';
                    string[] id2 = assesscombo.Text.Split(iddd);
                    string[] id3 = rubriccombo.Text.Split(iddd);
                    string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Update AssessmentComponent Set Name=@Name,RubricId=@RubricId,TotalMarks=@TotalMarks,DateUpdated=@DateUpdated,AssessmentId=@AssessmentId where Id=@Id", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    cmd.Parameters.AddWithValue("@Name", coursename.Text);
                    cmd.Parameters.AddWithValue("@RubricId", int.Parse(id3[0]));
                    cmd.Parameters.AddWithValue("@TotalMarks", textBox2.Text);
                    cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Today);
                    cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(id2[0]));
                    cmd.Parameters.AddWithValue("@Id", int.Parse(id));
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Edited");
                }
                else
                {
                    MessageBox.Show("Error! Some empty Field(s) ");
                }
            }
            showData();
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent where Name like '" + search_field.Text + "%' OR Id like '" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@Id", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
