﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Mark_Attendence_Form : Form
    {

        public Mark_Attendence_Form()
        {
            InitializeComponent();
            Load_Status_ToCombo();
            showData();
        }
        public void showData()
        {
  
            DateTime datetime = DateTime.Now;
            var shortDateValue = datetime.ToShortDateString();
            dataGridView1.Rows.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            SqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                this.dataGridView1.Rows.Insert(0, DR[0].ToString(), DR[1].ToString() + " " + DR[2], DR[5].ToString(),shortDateValue,"Present");
            }
            DR.Close();


        }
        private void Load_Status_ToCombo()
        {
            DateTime datetime = DateTime.Now;
            var shortDateValue = datetime.ToShortDateString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Name from Lookup where Category='ATTENDANCE_STATUS'", con);
            SqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                comboBox1.Items.Add(DR[0].ToString());
            }
            DR.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.CurrentRow.Cells[4].Value = comboBox1.SelectedItem.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Cells[4].Value.ToString() != "")
            {
                //select* from ClassAttendance ca where ca.AttendanceDate =
                DateTime date = DateTime.Parse(dataGridView1.CurrentRow.Cells[3].Value.ToString());
              //  MessageBox.Show(date.ToString());
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select ca.AttendanceDate from ClassAttendance ca where ca.AttendanceDate=@AttendanceDate", con);
                cmd.Parameters.AddWithValue("@AttendanceDate", date);
                SqlDataReader d = cmd.ExecuteReader();
                int count = 0;
                while (d.Read())
                {
                   // MessageBox.Show(d[0].ToString());
                    count++;
                }
                d.Close();
                if (count == 0)
                {
                    con = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("Insert into ClassAttendance values (@AttendanceDate)", con);
                    cmd.Parameters.AddWithValue("@AttendanceDate", date);
                    try
                    {
                        _ = cmd.ExecuteNonQuery();

                    }
                    catch (Exception ex) { MessageBox.Show("Marked Attendece" + ex); }
                    con = Configuration.getInstance().getConnection();
                    MessageBox.Show(date.ToString());
                    cmd = new SqlCommand("Select Max(Id) From ClassAttendance", con);
                    SqlDataReader DR = cmd.ExecuteReader();
                    int s = 0;
                    while (DR.Read())
                    {
                        s = int.Parse(DR[0].ToString());
                    }
                    MessageBox.Show(s.ToString());
                    foreach (DataGridViewRow dr in dataGridView1.Rows)
                    {
                        string c = dr.Cells[4].Value.ToString();
                        int att = 4;
                        if (c == "Present")
                        {
                            att = 1;
                        }
                        else if (c == "Absent")
                        {
                            att = 2;
                        }
                        else if (c == "Leave")
                        {
                            att = 3;
                        }
                        else if (c == "Late")
                        {
                            att = 4;
                        }

                        con = Configuration.getInstance().getConnection();
                        cmd = new SqlCommand("Insert into StudentAttendance values (@AttendanceId,@StudentId,@AttendanceStatus)", con);
                        cmd.Parameters.AddWithValue("@AttendanceId", s);
                        cmd.Parameters.AddWithValue("@StudentId", dr.Cells[0].Value.ToString());
                        cmd.Parameters.AddWithValue("@AttendanceStatus", att);
                        try
                        {
                            _ = cmd.ExecuteNonQuery();

                        }
                        catch { MessageBox.Show(dr.Cells[0].Value.ToString() + " Attendence's Markked Already "); }
                       
                    }
                    MessageBox.Show("Attendence Successfully Marked ");
                }
                else
                {
                    MessageBox.Show("Attendence Already Marked ");
                }
               

            }

        }

       
        private void Mark_Attendence_Form_Load(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            comboBox1.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime time = dateTimePicker1.Value.Date;
            var shortDateValue = time.ToShortDateString();
            foreach(DataGridViewRow dr in dataGridView1.Rows)
            {
               dr.Cells[3].Value= shortDateValue;
            }
        }
    }
}
