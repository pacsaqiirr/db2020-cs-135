﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using System.Threading;
using iTextSharp.text.html;
using Nest;

namespace Mid_Term
{
    public partial class Report : Form
    {
        public Report()
        {
            InitializeComponent();
            comboBox1.Text = "Select The Report";
        }
        private DataTable StudentMarks()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select P.[Student Name],P.[Total Marks],P.[Total Obtained Marks],cast((P.[Total Obtained Marks]/P.[Total Marks] *100) as numeric(18,2)) as [Obtained Percentage%] from( select N.[Student Name],Sum(N.[AssessmentComponent Marks]) as [Total Marks],Sum(N.[Obtained Marks]) as [Total Obtained Marks] from( select T.[Student Name],T.[Obtained Level],T.[Max Level],T.[AssessmentComponent Marks],cast(cast(T.[Obtained Level] as float)/T.[Max Level]*T.[AssessmentComponent Marks] as Numeric(18,2)) as [Obtained Marks] from( select s.FirstName+' '+s.LastName as [Student Name],rl.MeasurementLevel as [Obtained Level],(select max(eee.MeasurementLevel) from RubricLevel eee where eee.RubricId=r.Id) as [Max Level],ac.TotalMarks as [AssessmentComponent Marks] from StudentResult sr Join Student s on sr.StudentId=s.Id join AssessmentComponent ac on sr.AssessmentComponentId=ac.Id join RubricLevel rl on sr.RubricMeasurementId=rl.Id join Rubric r on rl.RubricId=r.Id ) as T) as N group by [Student Name] having [Student Name] is not null )as P Order by [Obtained Percentage%] desc", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            return dt;
        }
        private DataTable CLO_std_report()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select T.StudentId,T.CLO,T.AssessmentComponent,T.[Obtained Level],T.[Max Level],T.[AssessmentComponent Marks],cast((cast([Obtained Level] as float )/[Max Level])*[AssessmentComponent Marks] as numeric(18,2)) as [Obtained marks] from( select sr.StudentId,clo.Name as CLO,ac.Name as AssessmentComponent,rl.MeasurementLevel as [Obtained Level],(select max(rr.MeasurementLevel) from RubricLevel rr where rr.RubricId=r.Id) as [Max Level],ac.TotalMarks as [AssessmentComponent Marks] from StudentResult sr join RubricLevel rl on rl.Id=sr.RubricMeasurementId join Rubric r on r.Id=rl.RubricId join AssessmentComponent ac on ac.Id=sr.AssessmentComponentId join Clo clo on clo.Id=r.CloId)as T", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        private DataTable AttendenceReportByStudents()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" select N.Name as [Student Name],N.Presents,N.absents as Absents,N.TotalAttendance as [Total Attendance],CAST((cast(N.Presents as float) / N.TotalAttendance) * 100 AS NUMERIC(18, 2)) as [Precentage %]  from(select Distinct T.Id, T.Name, (Select count(*) from StudentAttendance ss where ss.StudentId = T.Id and ss.AttendanceStatus = 1) as Presents,(Select count(*) from StudentAttendance ss where ss.StudentId = T.Id and ss.AttendanceStatus = 2 ) absents,T.TotalAttendance from(select CONCAT(s.FirstName,' ',LastName) as Name,s.Id,(Select count(*) from ClassAttendance) as TotalAttendance from ClassAttendance ca join StudentAttendance sa on sa.AttendanceId = ca.Id join Student s on s.Id = sa.StudentId where sa.AttendanceStatus = 1) as T) as N", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        private DataTable Generate_Attendence_Report()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("Column1", "Date");
            dataGridView1.Columns.Add("Column1", "No. of Students Present");
            dataGridView1.Columns.Add("Column1", "No. of Students Absent");
            dataGridView1.Columns.Add("Column1", "No. of Student Late");
            dataGridView1.Columns.Add("Column1", "Students At Leave");
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select ac.AttendanceDate from ClassAttendance ac", con);
            SqlDataReader DR = cmd.ExecuteReader();
            List<string> AttendenceDates = new List<string>();
            while (DR.Read())
            {
                AttendenceDates.Add(DR[0].ToString());
                DateTime datetime = DateTime.Parse(DR[0].ToString());
                var shortDateValue = datetime.ToShortDateString();
                dataGridView1.Rows.Add(shortDateValue);

            }
            DR.Close();
            int i = 0;
            foreach (DataGridViewRow dr in dataGridView1.Rows) {
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select count(*) as [No. of Students Present] from ClassAttendance ca join StudentAttendance sa on sa.AttendanceId=ca.Id where sa.AttendanceStatus=1 group by ca.AttendanceDate having ca.AttendanceDate like cast('" + AttendenceDates[i] + "' as datetime)", con);
                DR = cmd.ExecuteReader();
                //List<string> AttendenceDates = new List<string>();
                string str = "";
                while (DR.Read())
                {
                    str= DR[0].ToString();
                }
                DR.Close();
                dr.Cells[1].Value = str;
                i++;
            }
            i = 0;
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select count(*) as [No. of Students Present] from ClassAttendance ca join StudentAttendance sa on sa.AttendanceId=ca.Id where sa.AttendanceStatus=2 group by ca.AttendanceDate having ca.AttendanceDate like cast('" + AttendenceDates[i] + "' as datetime)", con);
                DR = cmd.ExecuteReader();
                //List<string> AttendenceDates = new List<string>();
                string str = "0";
                try
                {
                    while (DR.Read())
                    {
                        str = DR[0].ToString();
                    }
                }
                catch { };
                DR.Close();
                dr.Cells[2].Value = str;
                i++;
            }
            i = 0;
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select count(*) as [No. of Students Present] from ClassAttendance ca join StudentAttendance sa on sa.AttendanceId=ca.Id where sa.AttendanceStatus=4 group by ca.AttendanceDate having ca.AttendanceDate like cast('" + AttendenceDates[i] + "' as datetime)", con);
                DR = cmd.ExecuteReader();
                //List<string> AttendenceDates = new List<string>();
                string str = "0";
                try
                {
                    while (DR.Read())
                    {
                        str = DR[0].ToString();
                    }
                }
                catch { };
                DR.Close();
                dr.Cells[3].Value = str;
                i++;
            }
            i = 0;
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select count(*) as [No. of Students Present] from ClassAttendance ca join StudentAttendance sa on sa.AttendanceId=ca.Id where sa.AttendanceStatus=3 group by ca.AttendanceDate having ca.AttendanceDate like cast('" + AttendenceDates[i] + "' as datetime)", con);
                DR = cmd.ExecuteReader();
                //List<string> AttendenceDates = new List<string>();
                string str = "0";
                try
                {
                    while (DR.Read())
                    {
                        str = DR[0].ToString();
                    }
                }
                catch { };
                DR.Close();
                dr.Cells[4].Value = str;
                i++;
            }

            var tb = new DataTable();
            tb.Columns.Add( "Date",typeof(string));
            tb.Columns.Add( "No. of Students Present", typeof(string));
            tb.Columns.Add( "No. of Students Absent", typeof(string));
            tb.Columns.Add( "No. of Student Late", typeof(string));
            tb.Columns.Add( "Students At Leave", typeof(string));
            var cell = new object[dataGridView1.Columns.Count];
            foreach (DataGridViewRow dataGridViewRow in dataGridView1.Rows)
            {
                for (int j = 0; j< dataGridViewRow.Cells.Count; j++)
                {
                    cell[j] = dataGridViewRow.Cells[j].Value;
                }
                tb.Rows.Add(cell);
            }
            return tb;
        }
        public DataTable AssessmentComponents()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            var tb = new DataTable();
            tb.Columns.Add("Assessment", typeof(string));
            tb.Columns.Add("Assessment Components", typeof(string));
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Title from Assessment", con);
            SqlDataReader DR = cmd.ExecuteReader();
            List<string> str = new List<string>();
            while (DR.Read())
            {
                str.Add(DR[0].ToString());
            }
            DR.Close();
            List<string> rubrics = new List<string>();
            foreach (string name in str)
            {
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select Name from AssessmentComponent where AssessmentId in (select Id from Assessment where Title like '%" + name.ToString() + "%')", con);
                DR = cmd.ExecuteReader();
                List<string> sss = new List<string>();
                string s = "";
                while (DR.Read())
                {
                    sss.Add(DR[0].ToString());
                }
                DR.Close();
                for (int i = 0; i < sss.Count; i++)
                {
                    if (i == sss.Count - 1)
                    {
                        s = s + sss[i];
                    }
                    else
                    {
                        s = s + sss[i] + ",";
                    }
                }
                rubrics.Add(s);
            }
            for (int j = 0; j < rubrics.Count; j++)
            {
                tb.Rows.Add(str[j], rubrics[j]);
            }

            return tb;

        }
        public DataTable Assessment_Report()
        {

            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("Column1", "Assessment");
            dataGridView1.Columns.Add("Column1", "AssessmentComponents");
            dataGridView1.Columns.Add("Column1", "No. of Students Passed");
            dataGridView1.Columns.Add("Column1", "Passing Criteria %");
            dataGridView1.Columns.Add("Column1", "Average Assessment Score");
            dataGridView1.Columns.Add("Column1", "Total ComponentMarks Marks");

            // Getting Assessments
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Title from Assessment", con);
            SqlDataReader DR = cmd.ExecuteReader();
            List<string> assessment_Title = new List<string>();
            while (DR.Read())
            {
                assessment_Title.Add(DR[0].ToString());
                dataGridView1.Rows.Add(DR[0].ToString());
            }
            DR.Close();

            //getting Assessment Component
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                string title=dr.Cells[0].Value.ToString();
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select ac.Name from Assessment a join AssessmentComponent ac on a.Id = ac.AssessmentId where a.Title like '%"+ title + "%'", con);
                DR = cmd.ExecuteReader();
                List<string> AssessmentComponent_name_each = new List<string>();
                while (DR.Read())
                {
                    AssessmentComponent_name_each.Add(DR[0].ToString());
                }
                DR.Close();
                string s = "";
                for(int j=0;j< AssessmentComponent_name_each.Count;j++)
                {
                    if(j==AssessmentComponent_name_each.Count-1)
                    {
                        s += AssessmentComponent_name_each[j];
                    }
                    else
                    {
                        s += AssessmentComponent_name_each[j] + ",";
                    }
                }
                dr.Cells[1].Value = s;
                dr.Cells[3].Value = 40;

            }
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                string title = dr.Cells[0].Value.ToString();
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select ac.Name from Assessment a join AssessmentComponent ac on a.Id = ac.AssessmentId where a.Title like '%" + title + "%'", con);
                DR = cmd.ExecuteReader();
                List<string> AssessmentComponent_name_each = new List<string>();
                while (DR.Read())
                {
                    AssessmentComponent_name_each.Add(DR[0].ToString());
                }
                DR.Close();
                string s = "";
                for (int j = 0; j < AssessmentComponent_name_each.Count; j++)
                {
                    if (j == AssessmentComponent_name_each.Count - 1)
                    {
                        s += AssessmentComponent_name_each[j];
                    }
                    else
                    {
                        s += AssessmentComponent_name_each[j] + ",";
                    }
                }
                dr.Cells[1].Value = s;
            }
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                string title = dr.Cells[0].Value.ToString();
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select COUNT(*) from(select R.StudentId from(select t.StudentId,t.Assessment,t.AssessmentComponent,t.AssessmentComponentMarks,t.ObtainedLevel,t.[Max Level],(cast(ObtainedLevel as float )/[Max Level])*AssessmentComponentMarks as [Obtained marks] from( select sr.StudentId as StudentId, a.Title as Assessment,ac.Name as AssessmentComponent,ac.TotalMarks as AssessmentComponentMarks,rl.MeasurementLevel as ObtainedLevel,(select max(rr.MeasurementLevel) from RubricLevel rr where rr.RubricId=r.Id) as [Max Level] from Assessment a join AssessmentComponent ac join StudentResult sr on sr.AssessmentComponentId=ac.Id on a.Id=ac.AssessmentId join RubricLevel rl on rl.Id=sr.RubricMeasurementId join Rubric r on r.Id=rl.RubricId where a.Title like '%"+title+"%') as  t)as R group by R.StudentId having sum(R.[Obtained marks])>(sum(R.AssessmentComponentMarks)* 0.4)) as w", con);
                DR = cmd.ExecuteReader();
                int students = 0;
                while (DR.Read())
                {
                    students = int.Parse(DR[0].ToString());
                }
                DR.Close();
               
                dr.Cells[2].Value = students;
            }
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                string title = dr.Cells[0].Value.ToString();
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select sum(ac.TotalMarks) from Assessment a join AssessmentComponent ac on a.Id=ac.AssessmentId group by a.Title", con);
                DR = cmd.ExecuteReader();
                int students = 0;
                while (DR.Read())
                {
                    students = int.Parse(DR[0].ToString());
                }
                DR.Close();
                dr.Cells[5].Value = students;
              
            }
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                string title = dr.Cells[0].Value.ToString();
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select (sum(R.[Obtained marks])/count([Obtained marks])) as [Average Marks] from(select t.StudentId,t.Assessment,t.AssessmentComponent,t.AssessmentComponentMarks,t.ObtainedLevel,t.[Max Level],(cast(ObtainedLevel as float )/[Max Level])*AssessmentComponentMarks as [Obtained marks],t.TotalMarks from( select a.TotalMarks,sr.StudentId as StudentId, a.Title as Assessment,ac.Name as AssessmentComponent,ac.TotalMarks as AssessmentComponentMarks,rl.MeasurementLevel as ObtainedLevel,(select max(rr.MeasurementLevel) from RubricLevel rr where rr.RubricId=r.Id) as [Max Level] from Assessment a join AssessmentComponent ac join StudentResult sr on sr.AssessmentComponentId=ac.Id on a.Id=ac.AssessmentId join RubricLevel rl on rl.Id=sr.RubricMeasurementId join Rubric r on r.Id=rl.RubricId where a.Title like '%"+title+"%') as  t)as R group by R.Assessment", con);
                DR = cmd.ExecuteReader();
                float students = 0;
                while (DR.Read())
                {
                    students = float.Parse(DR[0].ToString());
                }
                DR.Close();
                dr.Cells[4].Value = students.ToString("0.00"); 

            }
            var tb = new DataTable();
            tb.Columns.Add("Assessment", typeof(string));
            tb.Columns.Add("AssessmentComponents", typeof(string));
            tb.Columns.Add("No. of Students Passed", typeof(string));
            tb.Columns.Add("Passing Criteria %", typeof(string));
            tb.Columns.Add("Average Assessment Score", typeof(string));
            tb.Columns.Add("Total Ass_Comp Marks", typeof(string));
            var cell = new object[dataGridView1.Columns.Count];
            foreach (DataGridViewRow dataGridViewRow in dataGridView1.Rows)
            {
                for (int i = 0; i < dataGridViewRow.Cells.Count; i++)
                {
                    cell[i] = dataGridViewRow.Cells[i].Value;
                }
                tb.Rows.Add(cell);
            }
            return tb;
        }
        public DataTable CLO_Rubrics()
        {
            dataGridView1.DataSource = null;
            var tb = new DataTable();
            tb.Columns.Add("CLOs", typeof(string));
            tb.Columns.Add("Rubrics", typeof(string));
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Name from Clo", con);
            SqlDataReader DR = cmd.ExecuteReader();
            List<string> str = new List<string>();
            while (DR.Read())
            {
                str.Add(DR[0].ToString());
            }
            DR.Close();
            List<string> rubrics = new List<string>();
            foreach(string name in str)
            {
                con = Configuration.getInstance().getConnection();
                cmd = new SqlCommand("select Details from Rubric where CloId in (select Id from Clo where Name like '%"+name.ToString()+"%')", con);
                DR = cmd.ExecuteReader();
                List<string> sss = new List<string>();
                string s = "";
                while (DR.Read())
                {                  
                    sss.Add(DR[0].ToString());
                }
                DR.Close();
                for(int i=0;i<sss.Count;i++)
                {
                    if(i==sss.Count-1)
                    {
                        s = s + sss[i];
                    }
                    else
                    {
                        s = s + sss[i] + ",";
                    }
                }
                rubrics.Add(s);
            }
            for(int j=0;j<rubrics.Count;j++)
            {
                tb.Rows.Add(str[j], rubrics[j]);
            }
          //  MessageBox.Show(tb.Rows.Count.ToString());
            
            return tb;

        }
        public DataGridView Generate_CLo()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            if (comboBox1.SelectedIndex == 0)
            {
                ComboBox c = new ComboBox();
                dataGridView1.Columns.Clear();
                dataGridView1.Columns.Add("Column1", "CLO");
                dataGridView1.Columns.Add("Column1", "CLO Attained Checked In");
                dataGridView1.Columns.Add("Column1", "No. of Students Passed");
                dataGridView1.Columns.Add("Column1", "Passing Criteria %");
                dataGridView1.Columns.Add("Column1", "Average CLO Score");
                dataGridView1.Columns.Add("Column1", "Total Marks");
                dataGridView1.Columns.Add("Column1", "Total Obtained Marks");

                // clo adds
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select Name from Clo", con);
                SqlDataReader DR = cmd.ExecuteReader();
                while (DR.Read())
                {
                    c.Items.Add(DR[0].ToString());
                }
                DR.Close();
                for (int i = 0; i < c.Items.Count; i++)
                {
                    dataGridView1.Rows.Add(c.Items[i].ToString());
                }

                foreach (DataGridViewRow dr in dataGridView1.Rows)
                {
                    // MessageBox.Show(dr.Cells[0].Value.ToString());
                    string a = dr.Cells[0].Value.ToString();
                    con = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("select Distinct ac.Name  from AssessmentComponent ac join Rubric r on ac.RubricId=r.Id join Clo clo  on clo.Id=r.CloId where clo.Name like '%" + a + "%'", con);

                    DR = cmd.ExecuteReader();
                    string s = "";
                    List<string> ls = new List<string>();
                    while (DR.Read())
                    {
                        ls.Add(DR[0].ToString());
                    }
                    for (int l = 0; l < ls.Count; l++)
                    {
                        if (l == ls.Count - 1)
                        {
                            s = s + ls[l];
                        }
                        else
                        {
                            s = s + ls[l] + ",";
                        }
                    }
                    dr.Cells[1].Value = s;
                    dr.Cells[3].Value = 40;
                    DR.Close();

                }


                //foreach (DataGridViewRow dr in dataGridView1.Rows)
                //{
                //    List<float> mark = new List<float>();
                //    string a = dr.Cells[0].Value.ToString();
                //    con = Configuration.getInstance().getConnection();
                //    cmd = new SqlCommand("select  sr.StudentId,rl.MeasurementLevel,ass.TotalMarks,r.Id as RubricId,(select Max(rr.MeasurementLevel) from RubricLevel rr where rr.RubricId=r.Id) as MaxRubricLevel from StudentResult sr join RubricLevel rl on rl.Id=sr.RubricMeasurementId join AssessmentComponent ass on ass.Id=sr.AssessmentComponentId join Rubric r on r.Id=ass.RubricId join Clo clo on clo.Id=r.CloId where clo.Name like '%" + a + "'", con);
                //    DR = cmd.ExecuteReader();
                //    float obtainedrubric = 0;
                //    float totalrubric = 0;
                //    float totalassessmarks = 0;
                //    float answer = 0;
                //    float average = 0;
                //    float sum = 0;
                //    int i = 0;

                //    while (DR.Read())
                //    {
                //        obtainedrubric = float.Parse(DR[1].ToString());
                //        totalrubric = float.Parse(DR[4].ToString());
                //        totalassessmarks = float.Parse(DR[2].ToString());
                //        answer = (obtainedrubric / totalrubric) * totalassessmarks;

                //        //   MessageBox.Show("Studends Marks"+answer.ToString());
                //        mark.Add(answer);
                //        sum = sum + answer;
                //        i++;
                //    }
                //    average = (sum / i);
                //    dr.Cells[4].Value = average;
                //    //  MessageBox.Show("Average Marks" + average);
                //    int count = 0;
                //    foreach (float x in mark)
                //    {
                //        if (x >= average)
                //        {
                //            // MessageBox.Show(x.ToString());
                //            count++;
                //        }
                //    }
                //    dr.Cells[2].Value = count;
                //    // MessageBox.Show(count.ToString());

                //}
                //DR.Close();


                ///////////////////////////////
                ///


                foreach (DataGridViewRow dr in dataGridView1.Rows)
                {
                    List<float> mark = new List<float>();
                    string a = dr.Cells[0].Value.ToString();
                    con = Configuration.getInstance().getConnection();
                    cmd = new SqlCommand("select sr.StudentId,rl.MeasurementLevel,ass.TotalMarks,r.Id as RubricId,(select Max(rr.MeasurementLevel) from RubricLevel rr where rr.RubricId=r.Id) as MaxRubricLevel from StudentResult sr join RubricLevel rl on rl.Id=sr.RubricMeasurementId join AssessmentComponent ass on ass.Id=sr.AssessmentComponentId join Rubric r on r.Id=ass.RubricId join Clo clo on clo.Id=r.CloId where clo.Name like '%" + a + "'", con);
                    DR = cmd.ExecuteReader();
                    float obtainedrubric = 0;
                    float totalrubric = 0;
                    float totalassessmarks = 0;
                    float answer = 0;
                    float average = 0;
                    float sum = 0;
                    int i = 0;
                    List<int> stdids = new List<int>();
                    List<int> totalAssessmarks_studenr = new List<int>();
                    List<int> cccc = new List<int>();
                    int total_assessment_Marks = 0;
                    while (DR.Read())
                    {
                        stdids.Add(int.Parse(DR[0].ToString()));
                        obtainedrubric = float.Parse(DR[1].ToString());
                        totalrubric = float.Parse(DR[4].ToString());
                        totalassessmarks = float.Parse(DR[2].ToString());
                        total_assessment_Marks += int.Parse(DR[2].ToString());
                        cccc.Add(int.Parse(DR[2].ToString()));
                        answer = (obtainedrubric / totalrubric) * totalassessmarks;
                        mark.Add(answer);
                        sum = sum + answer;
                        i++;
                    }
                    float final_answer = 0;

                    List<int> finalstidents = new List<int>();
                    finalstidents = stdids.Distinct().ToList();
                    List<float> finalMarks = new List<float>();
                    //   MessageBox.Show(finalstidents.Count.ToString());
                    for (int k = 0; k < finalstidents.Count; k++)
                    {
                        //  MessageBox.Show("Final  Student Id " + finalstidents[k].ToString());
                        final_answer = 0;
                        int assTotalMarks = 0;
                        for (int j = 0; j < i; j++)
                        {
                            if (finalstidents[k] == stdids[j])
                            {
                                //   MessageBox.Show(" Dupli ST Student Id " + stdids[j].ToString());
                                final_answer += mark[j];
                                assTotalMarks += cccc[j];
                                //  MessageBox.Show(" Student 1 Total marks Adding " + assTotalMarks.ToString());

                            }
                        }
                        //  MessageBox.Show("Student " + finalstidents[k].ToString() + " " + assTotalMarks.ToString());
                        finalMarks.Add(final_answer);
                        totalAssessmarks_studenr.Add(assTotalMarks);



                    }
                    average = (sum / i);
                    //MessageBox.Show(total_assessment_Marks.ToString());
                    //MessageBox.Show(sum.ToString());
                    //MessageBox.Show(i.ToString());
                  
                    float percentage = (sum / i);
                    if(percentage==-2)
                    {

                    }
                    //   MessageBox.Show(percentage.ToString());
                    dr.Cells[4].Value = percentage.ToString("0.00");
                    //MessageBox.Show(percentage.ToString());
                    dr.Cells[5].Value = total_assessment_Marks;
                    dr.Cells[6].Value = sum.ToString("0.00");

                    int count = 0;
                    for (int h = 0; h < finalstidents.Count; h++)
                    {
                        //MessageBox.Show("Count " + totalAssessmarks_studenr.Count.ToString());
                        //MessageBox.Show("Marks " + finalMarks[h].ToString());
                        float half = (float)0.4;
                        float fiftypercent = totalAssessmarks_studenr[h] * half;
                        //MessageBox.Show("Total marks" + totalAssessmarks_studenr[h].ToString());
                        //MessageBox.Show("40 per " + fiftypercent.ToString());
                        if (finalMarks[h] > fiftypercent)
                        {

                            count++;
                        }
                    }
                    dr.Cells[2].Value = count;
                    DR.Close();


                }

            }

          //  MessageBox.Show(dataGridView1.Rows.Count.ToString());
               return dataGridView1;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex==0)
            {
                Generate_CLo();
            }
            if(comboBox1.SelectedIndex==1)
            {
                Assessment_Report();
            }
            if(comboBox1.SelectedIndex==2)
            {
                Generate_Attendence_Report();
            }
            if (comboBox1.SelectedIndex == 3)
            {
                StudentMarks();
            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                DataGridView tb1 = Generate_CLo();
                // MessageBox.Show(tb1.Rows.Count.ToString());
                var tb = new DataTable();
                tb.Columns.Add("CLO", typeof(string));
                tb.Columns.Add("CLO Attained Checked In", typeof(string));
                tb.Columns.Add("No. of Students Passed", typeof(string));
                tb.Columns.Add("Passing Criteria %", typeof(string));
                tb.Columns.Add("Average CLO Score of Class ", typeof(string));
                tb.Columns.Add("Total Marks", typeof(string));
                tb.Columns.Add("Obtained Marks", typeof(string));
                var cell = new object[tb1.Columns.Count];
                foreach (DataGridViewRow dataGridViewRow in tb1.Rows)
                {
                    for (int i = 0; i < dataGridViewRow.Cells.Count; i++)
                    {
                        cell[i] = dataGridViewRow.Cells[i].Value;
                    }
                    tb.Rows.Add(cell);
                }
                Thread t = new Thread((ThreadStart)(() =>
                {
                    OpenFileDialog op = new OpenFileDialog();
                    string folderPath = "";
                    op.ValidateNames = false;
                    op.CheckFileExists = false;
                    op.CheckPathExists = true;
                    op.FileName = "Clo_Report.pdf";

                    if (op.ShowDialog() == DialogResult.OK)
                    {
                        folderPath = Path.GetDirectoryName(op.FileName);
                        FileStream fs = new FileStream(@"" + folderPath + @"\Clo_Report.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                        Document doc = new Document();
                        doc.SetPageSize(iTextSharp.text.PageSize
                            .A4);
                        PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                        doc.Open();

                    // Report Header
                    BaseFont bfothead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
                        Font fnt1 = new(bfothead, 16, 1, BaseColor.BLACK);
                        Font fnt3 = new(bfothead, 14, 0, BaseColor.BLACK);
                        Font fnt2 = new(bfothead, 14, 1, BaseColor.BLACK);
                        Paragraph prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_CENTER;
                        prg.Add(new Chunk("Course Learning Outcome Attainment Report", fnt1));
                        doc.Add(prg);                 
                    Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.DARK_GRAY, Element.ALIGN_LEFT, 1)));
                        doc.Add(p);
                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("\nCLO and Rubrics \n\n", fnt2));
                        doc.Add(prg);
                        DataTable t = CLO_Rubrics();
#pragma warning disable CS0612 // Type or member is obsolete
                    BaseColor myColor = WebColors.GetRGBColor("#201F2C");
#pragma warning restore CS0612 // Type or member is obsolete
                    PdfPTable table1 = new PdfPTable(t.Columns.Count);
                        table1.WidthPercentage = 100;
                        table1.TotalWidth = 500;
                    //table header
                    fnt1 = new Font(bfothead, 10, 1, BaseColor.BLACK);
                        for (int i = 0; i < t.Columns.Count; i++)
                        {
                            PdfPCell cell = new PdfPCell();
                           // cell.BackgroundColor = myColor;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            
                            cell.AddElement(new Chunk(t.Columns[i].ColumnName.ToUpper(), fnt1));
                            table1.AddCell(cell);
                        }
                    //table data
                    for (int i = 0; i < t.Rows.Count; i++)
                        {
                            for (int j = 0; j < t.Columns.Count; j++)
                            {
                                table1.AddCell(t.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table1);

                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("\nCLO's Wise Class Result\n\n", fnt2));
                        doc.Add(prg);

                    // write second table
                    PdfPTable table2 = new PdfPTable(tb.Columns.Count);
                        table2.WidthPercentage = 100;
                        table2.TotalWidth = 500;
                        //table header
                        fnt1 = new Font(bfothead, 10, 1, BaseColor.BLACK);
                        for (int i = 0; i < tb.Columns.Count; i++)
                        {
                            PdfPCell cel = new PdfPCell();
                            cel.HorizontalAlignment = Element.ALIGN_CENTER;
                            cel.AddElement(new Chunk(tb.Columns[i].ColumnName.ToUpper(), fnt1));
                            table2.AddCell(cel);
                        }
                    //table data
                    for (int i = 0; i < tb.Rows.Count; i++)
                        {
                            for (int j = 0; j < tb.Columns.Count; j++)
                            {
                                table2.AddCell(tb.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table2);
                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        string str = "\nStudent Result CLO Sise\n\n";
                        prg.Add(new Chunk(str,fnt2));
                        doc.Add(prg);

                        DataTable clo = CLO_std_report();
                        PdfPTable table3 = new PdfPTable(tb.Columns.Count);
                        table3.WidthPercentage = 100;
                        table3.TotalWidth = 500;
                        //table header
                      
                        for (int i = 0; i < clo.Columns.Count; i++)
                        {
                            PdfPCell cel = new PdfPCell();;
                            cel.HorizontalAlignment = Element.ALIGN_CENTER;
                            cel.AddElement(new Chunk(clo.Columns[i].ColumnName.ToUpper(), fnt1));
                            table3.AddCell(cel);
                        }
                        //table data
                        for (int i = 0; i < clo.Rows.Count; i++)
                        {
                            for (int j = 0; j < clo.Columns.Count; j++)
                            {
                                table3.AddCell(clo.Rows[i][j].ToString());
                            }
                        }

                        doc.Add(table3);
                        doc.Close();
                        writer.Close();
                        fs.Close();
                    }

                }));
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                MessageBox.Show("PDF Generated");
            }
            if(comboBox1.SelectedIndex==1)
            {
                DataTable tb = AssessmentComponents();
                DataTable tb1 = Assessment_Report();
                Thread t = new Thread((ThreadStart)(() =>
                {
                    OpenFileDialog op = new OpenFileDialog();
                    string folderPath = "";
                    op.ValidateNames = false;
                    op.CheckFileExists = false;
                    op.CheckPathExists = true;
                    op.FileName = "Assessment_Report.pdf";

                    if (op.ShowDialog() == DialogResult.OK)
                    {
                        folderPath = Path.GetDirectoryName(op.FileName);
                        FileStream fs = new FileStream(@"" + folderPath + @"\Assessment_Report.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                        Document doc = new Document();
                        doc.SetPageSize(iTextSharp.text.PageSize
                            .A4);
                        PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                        doc.Open();
                        doc.AddTitle("Assessment Report");
                        // Report Header
                        BaseFont bfothead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
                        Font fnt1 = new(bfothead, 16, 1, BaseColor.BLACK);
                        Font fnt3 = new(bfothead, 14, 0, BaseColor.BLACK);
                        Paragraph prg = new Paragraph();                   
                        prg.Alignment = Element.ALIGN_CENTER;
                        Font fnt2 = new Font(bfothead, 14, 1, BaseColor.BLACK);
                        prg.Add(new Chunk("Assessment Wise Report", fnt1));
                        doc.Add(prg);
                        Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.DARK_GRAY, Element.ALIGN_LEFT, 1)));
                        doc.Add(p);
                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("Assessment and AssessmentComponents \n\n", fnt2));
                        doc.Add(prg);
                       
#pragma warning disable CS0612 // Type or member is obsolete
                        BaseColor myColor = WebColors.GetRGBColor("#22272D");
#pragma warning restore CS0612 // Type or member is obsolete
                        PdfPTable table1 = new PdfPTable(tb.Columns.Count);
                        table1.WidthPercentage = 100;
                        table1.TotalWidth = 500;
                        fnt1 = new Font(bfothead, 14, 1, BaseColor.BLACK);
                        Font fnt5 = new Font(bfothead, 10, 1, BaseColor.BLACK);
                        //table header
                        Font fnt4 = new Font(bfothead, 10, 1, BaseColor.BLACK);
                        for (int i = 0; i < tb.Columns.Count; i++)
                        {
                            PdfPCell cell = new PdfPCell();
                           // cell.BackgroundColor = myColor;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.AddElement(new Chunk(tb.Columns[i].ColumnName.ToUpper(), fnt5));
                            table1.AddCell(cell);
                        }
                        //table data
                        for (int i = 0; i < tb.Rows.Count; i++)
                        {
                            for (int j = 0; j < tb.Columns.Count; j++)
                            {
                                table1.AddCell(tb.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table1);

                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("\nAssessment Wise Class Result\n\n", fnt2));
                        doc.Add(prg);

                        PdfPTable table2 = new PdfPTable(tb1.Columns.Count);
                        table2.WidthPercentage = 100;
                        table2.TotalWidth = 500;
                        fnt1 = new Font(bfothead, 10, 1, BaseColor.WHITE);
                        for (int i = 0; i < tb1.Columns.Count; i++)
                        {
                            PdfPCell cel = new PdfPCell();
                            cel.HorizontalAlignment = Element.ALIGN_CENTER;
                            cel.AddElement(new Chunk(tb1.Columns[i].ColumnName.ToUpper(), fnt4));
                            table2.AddCell(cel);
                        }
                        //table data
                        for (int i = 0; i < tb1.Rows.Count; i++)
                        {
                            for (int j = 0; j < tb1.Columns.Count; j++)
                            {
                                table2.AddCell(tb1.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table2);
                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("\nMore Detail About Students Result Assessment Wise\n\n", fnt2));
                        doc.Add(prg);

                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("select R.StudentId,R.Assessment,R.AssessmentComponent,R.AssessmentComponentMarks,R.[Max Level],cast(R.[Obtained marks] as Numeric(18,2)) as [Obtained Marks],R.ObtainedLevel from(select t.StudentId,t.Assessment,t.AssessmentComponent,t.AssessmentComponentMarks,t.ObtainedLevel,t.[Max Level],(cast(ObtainedLevel as float )/[Max Level])*AssessmentComponentMarks as [Obtained marks],t.TotalMarks from( select a.TotalMarks,sr.StudentId as StudentId, a.Title as Assessment,ac.Name as AssessmentComponent,ac.TotalMarks as AssessmentComponentMarks,rl.MeasurementLevel as ObtainedLevel,(select max(rr.MeasurementLevel) from RubricLevel rr where rr.RubricId=r.Id) as [Max Level] from Assessment a join AssessmentComponent ac join StudentResult sr on sr.AssessmentComponentId=ac.Id on a.Id=ac.AssessmentId join RubricLevel rl on rl.Id=sr.RubricMeasurementId join Rubric r on r.Id=rl.RubricId ) as  t)as R", con);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        PdfPTable table3 = new PdfPTable(dt.Columns.Count);
                        table3.WidthPercentage = 100;
                        table3.TotalWidth = 500;
                        //table header
                        fnt1 = new Font(bfothead, 10, 1, BaseColor.WHITE);
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            PdfPCell cel = new PdfPCell();
                            cel.HorizontalAlignment = Element.ALIGN_CENTER;
                            cel.AddElement(new Chunk(dt.Columns[i].ColumnName.ToUpper(), fnt4));
                            table3.AddCell(cel);
                        }
                        //table data
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                table3.AddCell(dt.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table3);
                        doc.Close();
                        writer.Close();
                        fs.Close();
                    }

                }));
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                MessageBox.Show("PDF Generated");
            }
            if(comboBox1.SelectedIndex==2)
            {
                DataTable tb = Generate_Attendence_Report();             
                Thread t = new Thread((ThreadStart)(() =>
                {
                    OpenFileDialog op = new OpenFileDialog();
                    string folderPath = "";
                    op.ValidateNames = false;
                    op.CheckFileExists = false;
                    op.CheckPathExists = true;
                    op.FileName = "Attendance_Report.pdf";

                    if (op.ShowDialog() == DialogResult.OK)
                    {
                        folderPath = Path.GetDirectoryName(op.FileName);
                        FileStream fs = new FileStream(@"" + folderPath + @"\Attendance_Report.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                        Document doc = new Document();
                        doc.SetPageSize(iTextSharp.text.PageSize
                            .A4);
                        PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                        doc.Open();

                        // Report Header
                        BaseFont bfothead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
                        Font fnt1 = new(bfothead, 16, 1, BaseColor.BLACK);
                        Font fnt3 = new(bfothead, 14, 0, BaseColor.BLACK);
                        Paragraph prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_CENTER;
                        Font fnt2 = new Font(bfothead, 14, 1, BaseColor.BLACK);
                        prg.Add(new Chunk("Class Attendance Report", fnt1));
                        doc.Add(prg);
                        Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.DARK_GRAY, Element.ALIGN_LEFT, 1)));
                        doc.Add(p);
                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("Date Wise Attendance Report \n\n", fnt2));
                        doc.Add(prg);

#pragma warning disable CS0612 // Type or member is obsolete
                        BaseColor myColor = WebColors.GetRGBColor("#201F2C");
#pragma warning restore CS0612 // Type or member is obsolete
                        PdfPTable table1 = new PdfPTable(tb.Columns.Count);
                        Font fnt5 = new Font(bfothead, 10, 1, BaseColor.BLACK);
                        table1.WidthPercentage = 100;
                        table1.TotalWidth = 500;
                        //table header
                        fnt1 = new Font(bfothead, 10, 1, BaseColor.WHITE);
                        for (int i = 0; i < tb.Columns.Count; i++)
                        {
                            PdfPCell cell = new PdfPCell();
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.AddElement(new Chunk(tb.Columns[i].ColumnName.ToUpper(), fnt5));
                            table1.AddCell(cell);
                        }
                        //table data
                        for (int i = 0; i < tb.Rows.Count; i++)
                        {
                            for (int j = 0; j < tb.Columns.Count; j++)
                            {
                                table1.AddCell(tb.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table1);
                        var tb1 = AttendenceReportByStudents();
                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("\nStudent Wise Attendance Report\n\n", fnt2));
                        doc.Add(prg);

                        fnt5 = new Font(bfothead, 10, 1, BaseColor.BLACK);
                        PdfPTable table2 = new PdfPTable(tb1.Columns.Count);
                        table2.WidthPercentage = 100;
                        table2.TotalWidth = 500;
                        fnt1 = new Font(bfothead, 10, 1, BaseColor.WHITE);
                        for (int i = 0; i < tb1.Columns.Count; i++)
                        {
                            PdfPCell cel = new PdfPCell();
                            cel.HorizontalAlignment = Element.ALIGN_CENTER;
                            cel.AddElement(new Chunk(tb1.Columns[i].ColumnName.ToUpper(), fnt5));
                            table2.AddCell(cel);
                        }
                        //table data
                        for (int i = 0; i < tb1.Rows.Count; i++)
                        {
                            for (int j = 0; j < tb1.Columns.Count; j++)
                            {
                                table2.AddCell(tb1.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table2);
                     
                        doc.Close();
                        writer.Close();
                        fs.Close();
                    }

                }));
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                MessageBox.Show("PDF Generated");

            }
            if(comboBox1.SelectedIndex==3)
            { 

                DataTable tb = StudentMarks();
                Thread t = new Thread((ThreadStart)(() =>
                {
                    OpenFileDialog op = new OpenFileDialog();
                    string folderPath = "";
                    op.ValidateNames = false;
                    op.CheckFileExists = false;
                    op.CheckPathExists = true;
                    op.FileName = "StudentResult.pdf";

                    if (op.ShowDialog() == DialogResult.OK)
                    {
                        folderPath = Path.GetDirectoryName(op.FileName);
                        FileStream fs = new FileStream(@"" + folderPath + @"\StudentResult.pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                        Document doc = new Document();
                        doc.SetPageSize(iTextSharp.text.PageSize
                            .A4);
                        PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                        doc.Open();

                        // Report Header
                        BaseFont bfothead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.NOT_EMBEDDED);
                        Font fnt1 = new(bfothead, 16, 1, BaseColor.BLACK);
                        Font fnt3 = new(bfothead, 14, 0, BaseColor.BLACK);
                        Paragraph prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_CENTER;
                        Font fnt2 = new Font(bfothead, 14, 1, BaseColor.BLACK);
                        prg.Add(new Chunk("Student Overall Result Report", fnt1));
                        doc.Add(prg);
                        Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.DARK_GRAY, Element.ALIGN_LEFT, 1)));
                        doc.Add(p);
                        prg = new Paragraph();
                        prg.Alignment = Element.ALIGN_LEFT;
                        prg.Add(new Chunk("Student Result \n\n", fnt2));
                        doc.Add(prg);

#pragma warning disable CS0612 // Type or member is obsolete
                        BaseColor myColor = WebColors.GetRGBColor("#201F2C");
#pragma warning restore CS0612 // Type or member is obsolete
                        PdfPTable table1 = new PdfPTable(tb.Columns.Count);
                        Font fnt5 = new Font(bfothead, 10, 1, BaseColor.BLACK);
                        table1.WidthPercentage = 100;
                        table1.TotalWidth = 500;
                        //table header
                        fnt1 = new Font(bfothead, 10, 1, BaseColor.WHITE);
                        for (int i = 0; i < tb.Columns.Count; i++)
                        {
                            PdfPCell cell = new PdfPCell();
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.AddElement(new Chunk(tb.Columns[i].ColumnName.ToUpper(), fnt5));
                            table1.AddCell(cell);
                        }
                        //table data
                        for (int i = 0; i < tb.Rows.Count; i++)
                        {
                            for (int j = 0; j < tb.Columns.Count; j++)
                            {
                                table1.AddCell(tb.Rows[i][j].ToString());
                            }
                        }
                        doc.Add(table1);
                        
                        doc.Close();
                        writer.Close();
                        fs.Close();
                    }

                }));
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                MessageBox.Show("PDF Generated");

            }

        }
        }
    }
    

