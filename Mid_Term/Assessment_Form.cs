﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Assessment_Form : Form
    {
        public Assessment_Form()
        {
            InitializeComponent();
            showData();
        }

        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            coursename.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            coursename.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox1.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title,@DateCreated,@TotalMarks,@TotalWeightage)", con);
                cmd.Parameters.AddWithValue("@Title", coursename.Text);
                cmd.Parameters.AddWithValue("@DateCreated", DateTime.Today);
                cmd.Parameters.AddWithValue("@TotalMarks",textBox1.Text);
                cmd.Parameters.AddWithValue("@TotalWeightage", textBox2.Text);
                try
                {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Assessment saved");
                }
                catch { MessageBox.Show("Student Already Added"); }


            }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }
            showData();
        }
        public bool is_empty()
        {
            if (coursename.Text == "")
            {
                return false;
            }
            else if (textBox1.Text == "")
            {
                return false;
            }
            else if (textBox2.Text == "")
            {
                return false;
            }

            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Assessment Set Title=@Title,TotalMarks=@TotalMarks,TotalWeightage=@TotalWeightage where Id=@Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@Title", coursename.Text);
                cmd.Parameters.AddWithValue("@TotalMarks", textBox1.Text);
                cmd.Parameters.AddWithValue("@TotalWeightage", textBox2.Text);
                cmd.Parameters.AddWithValue("@Id", id);
                _ = cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            else
            {
                MessageBox.Show("Error! Some empty Field(s) ");
            }
            showData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete StudentResult where AssessmentComponentId in (select Id from AssessmentComponent where AssessmentId=@AssessmentId) ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@AssessmentId", id);
            _ = cmd.ExecuteNonQuery();

            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete AssessmentComponent where AssessmentId=@AssessmentId", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@AssessmentId", id);
            _ = cmd.ExecuteNonQuery();

            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete Assessment where Id=@Id", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@Id", id);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");

            showData();
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment where Title like '" + search_field.Text + "%' OR Id like '" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@Id", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
