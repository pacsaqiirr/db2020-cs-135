﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Evaluation_Form : Form
    {
        public Evaluation_Form()
        {
            InitializeComponent();
            showData();
            showData1();
            
            Load_assessment_ToCombo();
        }
        private void Load_RubricMeasurementId_ToCombo()
        {
            measurmentcombo.Items.Clear();
            Char iddd = '.';
            string[] id = componentcombo.Text.Split(iddd);
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel where RubricId in (select RubricId from AssessmentComponent where Id="+int.Parse(id[0])+")", con);
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {
                s += DR[0].ToString() + "." + DR[2].ToString();
                measurmentcombo.Items.Add(s);
                s = "";
            }
            DR.Close();
        }
        private void Load_AssessmentCompoentId_ToCombo()
        {
            componentcombo.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,Name from AssessmentComponent where AssessmentId in (select Id from Assessment where Title like '%"+comboBox1.SelectedItem.ToString()+"%')", con);
            SqlDataReader DR = cmd.ExecuteReader();
            string s = "";
            while (DR.Read())
            {
                s += DR[0].ToString() + "." + DR[1].ToString();
                componentcombo.Items.Add(s);
                s = "";
            }
            DR.Close();
        }
        private void Load_assessment_ToCombo()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Title from Assessment ", con);
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox1.Items.Add(DR[0]);

            }
            DR.Close();
        }
        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from StudentResult", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        public void showData1()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,concat(FirstName,' ',LastName) as Name,RegistrationNumber from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_AssessmentCompoentId_ToCombo();
        }

        private void componentcombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_RubricMeasurementId_ToCombo();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from StudentResult where StudentId like '" + textBox1.Text + "%'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,concat(FirstName,' ',LastName) as Name,RegistrationNumber from Student where concat(FirstName,' ',LastName) like '" + search_field.Text + "%' OR Id like '" + search_field.Text + "%'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                Char iddd = '.';
                string[] id2 = componentcombo.Text.Split(iddd);
                string[] id3 = measurmentcombo.Text.Split(iddd);
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into StudentResult values (@StudentId,@AssessmentComponentId,@RubricMeasurementId,@Evaluationdate)", con);
                cmd.Parameters.AddWithValue("@StudentId", int.Parse(dataGridView2.CurrentRow.Cells[0].Value.ToString()));
                cmd.Parameters.AddWithValue("@AssessmentComponentId",int.Parse(id2[0]));
                cmd.Parameters.AddWithValue("@RubricMeasurementId",int.Parse(id3[0]));
                cmd.Parameters.AddWithValue("@Evaluationdate", DateTime.Today);
                try
                {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Stdent Evaluated");
                }
                catch { MessageBox.Show("Student Already Evaluated"); }


            }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }
            showData();
        }
        public bool is_empty()
        {
            if (comboBox1.Text == "Select Id" || comboBox1.Text == "")
            {
                return false;
            }
            else if (componentcombo.Text == "Select Id" || componentcombo.Text == "" )
            {
                return false;
            }
            else if (measurmentcombo.Text == "Select Id" || measurmentcombo.Text == "")
            {
                return false;
            }
            return true;
        }
        public void clear_Grid()
        {
            comboBox1.Text = "Select Id";
            componentcombo.Text = "Select Id";
            measurmentcombo.Text = "Select Id";
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clear_Grid();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                Char iddd = '.';
                string[] id2 = componentcombo.Text.Split(iddd);
                string[] id3 = measurmentcombo.Text.Split(iddd);
                string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update StudentResult Set AssessmentComponentId=@AssessmentComponentId,RubricMeasurementId=@RubricMeasurementId,Evaluationdate=@Evaluationdate where StudentId=@StudentId and AssessmentComponentId="+ dataGridView1.CurrentRow.Cells[1].Value.ToString(), con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@AssessmentComponentId",int.Parse(id2[0]) );
                cmd.Parameters.AddWithValue("@RubricMeasurementId", int.Parse(id3[0]));
                cmd.Parameters.AddWithValue("@Evaluationdate", DateTime.Now);
                cmd.Parameters.AddWithValue("@StudentId", id);
                try {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Edited");
                }
                catch (Exception ex) { MessageBox.Show("Evaluation Already Exist"); }
            }
            else
            {
                MessageBox.Show("Error! Some empty Field(s) ");
            }
            showData();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {   
            componentcombo.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            measurmentcombo.Text= dataGridView1.CurrentRow.Cells[2].Value.ToString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Title from Assessment where Id in(select AssessmentId from AssessmentComponent where Id=" + componentcombo.Text + ")", con);
            SqlDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                comboBox1.Text = DR[0].ToString();
            }
            DR.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete StudentResult where StudentId=@StudentId and AssessmentComponentId=@AssessmentComponentId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@StudentId", id);
            cmd.Parameters.AddWithValue("@AssessmentComponentId", dataGridView1.CurrentRow.Cells[1].Value.ToString());
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
            showData();
        }
    }
   
}
