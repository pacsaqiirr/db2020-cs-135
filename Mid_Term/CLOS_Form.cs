﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class CLOS_Form : Form
    {
        public CLOS_Form()
        {
            InitializeComponent();
            showData();
        }

        public void Clo_Delete()
        {
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete StudentResult where AssessmentComponentId in (select Id from AssessmentComponent where RubricId in (select Id from Rubric where CloId=@CloId))", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@CloId", id);
            _ = cmd.ExecuteNonQuery();

            //ASSESSMENT COMPONENT
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete AssessmentComponent where RubricId in (select Id from Rubric where CloId=@CloId)", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@CloId", id);
            _ = cmd.ExecuteNonQuery();

            //RUBRIC LEVEL
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete RubricLevel where RubricId in (select Id from Rubric where CloId=@CloId)", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@CloId", id);
            _ = cmd.ExecuteNonQuery();

            //DELELTING RUBRICS
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete Rubric where CloId=@CloId", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@CloId", id);
            _ = cmd.ExecuteNonQuery();

            //DELETING CLO
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete Clo where Id=@Id", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@Id", id);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");

            showData();
        }
        public void showData()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void CLOS_Form_Load(object sender, EventArgs e)
        {

        }
        public void clear_Grid()
        {
            coursename.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clear_Grid();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Clo values (@Name,@DateCreated,@DateUpdated)", con);
                cmd.Parameters.AddWithValue("@Name", coursename.Text);
                cmd.Parameters.AddWithValue("@DateCreated", DateTime.Today);
                cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Today);
                try
                {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Clo saved");
                }
                catch { MessageBox.Show("Student Already Added"); }


            }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }
            showData();
        }
        public bool is_empty()
        {
            if (coursename.Text=="")
            {
                return false;
            }


            return true;
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            coursename.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();        
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                string id= dataGridView1.CurrentRow.Cells[0].Value.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Clo Set Name=@Name, DateUpdated=@DateUpdated where Id=@Id", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@Name", coursename.Text);
                cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Today);
                cmd.Parameters.AddWithValue("@Id", id);
                _ = cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            else
            {
                MessageBox.Show("Error! Some empty Field(s) ");
            }
            showData();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Clo_Delete();
        }

        private void search_field_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo where Name like '" + search_field.Text + "%' OR Id like '" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@Id", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    
    }
}
