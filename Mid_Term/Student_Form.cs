﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Term
{
    public partial class Student_Form : Form
    {
        public Student_Form()
        {
            InitializeComponent();
            showData();
            Load_ComboBox();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student where RegistrationNumber like '%" + search_field.Text + "%' OR FirstName like '%" + search_field.Text + "%' OR LastName like '%" + search_field.Text + "%'", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", search_field.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Student_Form_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from StudentResult where StudentId=@StudentId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@StudentId", id);
            _ = cmd.ExecuteNonQuery();

            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete from StudentAttendance where StudentId=@StudentId", con);
            da = new SqlDataAdapter(cmd);
            cmd.Parameters.AddWithValue("@StudentId", id);
            _ = cmd.ExecuteNonQuery();


           /// MessageBox.Show(reg.Text);
            con = Configuration.getInstance().getConnection();
            cmd = new SqlCommand("Delete Student where Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", id);
            _ = cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");



            showData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                int status= int.Parse(comboBox1.SelectedIndex.ToString());
                int intstatus = 6;
                if(status == 0)
                {                 
                    intstatus = 5;
                }


                var con = Configuration.getInstance().getConnection();
              
                SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName,@LastName,@Contact,@Email,@RegistrationNumber,@Status)", con);
                cmd.Parameters.AddWithValue("@FirstName", FirstName.Text);
                cmd.Parameters.AddWithValue("@LastName", LastName.Text);
                cmd.Parameters.AddWithValue("@Contact", contact.Text);
                cmd.Parameters.AddWithValue("@Email", email.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", reg.Text);             
                cmd.Parameters.AddWithValue("@Status", intstatus);
                try
                {
                    _ = cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                }
                catch { MessageBox.Show("Student Already Added"); }


            }
            else
            {
                MessageBox.Show("Some empty Field(s) ");
            }
            showData();
        }
         public bool is_empty()
        {
            if(reg.Text == "" || FirstName.Text == "" || contact.Text == "" || email.Text=="" || LastName.Text=="")
            {
                return false;
            }

          
            return true;
        }
        public void clear_Grid()
        {
            reg.Text = "";
            FirstName.Text = "";
            contact.Text = "";
            email.Text  = "";
            LastName.Text = "";
            comboBox1.SelectedIndex = 0;
        }


        public void showData(){
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            clear_Grid();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (is_empty())
            {
                int status = int.Parse(comboBox1.SelectedIndex.ToString());
                int intstatus = 6;
                if (status == 0)
                {
                    intstatus = 5;
                }
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Student Set FirstName=@FirstName, LastName=@LastName,Contact=@Contact,Email=@Email,RegistrationNumber=@RegistrationNumber,Status=@Status where RegistrationNumber=@RegistrationNumber", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@FirstName", FirstName.Text);
                cmd.Parameters.AddWithValue("@LastName", LastName.Text);
                cmd.Parameters.AddWithValue("@Contact", contact.Text);
                cmd.Parameters.AddWithValue("@Email", email.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", reg.Text);
                cmd.Parameters.AddWithValue("@Status", intstatus);
                _ = cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Edited");
            }
            else
            {
                MessageBox.Show("Error! Some empty Field(s) ");
            }
            showData();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            FirstName.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            LastName.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            contact.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            email.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            reg.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
            string status= dataGridView1.CurrentRow.Cells[6].Value.ToString();
            if (status=="5")
            {
                comboBox1.SelectedIndex=0;
            }
            else
            {
                comboBox1.SelectedIndex = 1;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void Load_ComboBox()
        {
            comboBox1.ForeColor = Color.White;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name from Lookup where Category like '%STUDENT_STATUS%' ", con);
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                comboBox1.Items.Add(DR[0]);

            }
            DR.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
